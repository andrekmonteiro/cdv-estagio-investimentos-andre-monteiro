from django.urls import path
from .views import PaginaInicial, Manutencao, Incluir, Editar, AtualizarEtapa, PessoaCreate
from .views import PessoaList

urlpatterns = [
    path('clubedovalor/', PaginaInicial.as_view(), name = 'inicio'),
    path('manutencao/', Manutencao.as_view(), name = 'manutencao'),
    path('incluir/', PessoaCreate.as_view(), name = 'incluir_cliente'),
    path('editar/', Editar.as_view(), name = 'editar'),
    path('atualizaretapa/', AtualizarEtapa.as_view(), name = 'atualizaretapa'),
    path('listar/', PessoaList.as_view(), name = 'listar'),
]