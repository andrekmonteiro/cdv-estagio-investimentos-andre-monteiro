# Generated by Django 3.2.6 on 2021-08-12 00:25

import cpf_field.models
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pessoa',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=50)),
                ('cpf', cpf_field.models.CPFField(max_length=14, verbose_name='CPF')),
                ('etapa_cliente', models.CharField(choices=[('Aguardando assinatura de documentos', 'Aguardando assinatura de documentos'), ('Aguardando transferência de recursos', 'Aguardando transferência de recursos'), ('Gestão de patrimônio ativa', 'Gestão de patrimônio ativa')], max_length=36)),
                ('data_criacao', models.DateTimeField(auto_now_add=True)),
                ('data_atualizacao', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
