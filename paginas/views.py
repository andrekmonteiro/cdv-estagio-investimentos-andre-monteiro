from django.urls.conf import path
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.core.exceptions import ValidationError
from .models import Pessoa
from django.urls import reverse_lazy

class PaginaInicial(TemplateView):
    template_name = "index.html"

class Manutencao(TemplateView):
    template_name = "manutencao.html"

class Incluir(TemplateView):
    template_name = "incluir.html"

class Editar(TemplateView):
    template_name =  "editar.html"

class AtualizarEtapa(TemplateView):
    template_name = "atualizaretapa.html"

class PessoaCreate(CreateView):
    model = Pessoa
    fields = ['nome','cpf', 'etapa_cliente']
    template_name = 'incluir.html'
    success_url = reverse_lazy('inicio')

class PessoaList(ListView): 
    model = Pessoa
    template_name = "listar.html"
    