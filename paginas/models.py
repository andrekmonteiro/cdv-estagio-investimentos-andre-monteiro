from django.db import models
from cpf_field.models import CPFField

class Pessoa(models.Model):

    ETAPA = (
        ('Aguardando assinatura de documentos', 'Aguardando assinatura de documentos'),
        ('Aguardando transferência de recursos', 'Aguardando transferência de recursos'),
        ('Gestão de patrimônio ativa', 'Gestão de patrimônio ativa'),
    )
    nome = models.CharField(max_length=50)
    cpf = CPFField('CPF') 
    etapa_cliente = models.CharField(max_length=36, choices = ETAPA) 
    data_criacao = models.DateTimeField(auto_now_add=True)
    data_atualizacao = models.DateTimeField(auto_now=True)
